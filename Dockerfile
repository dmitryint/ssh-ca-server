FROM python:2-alpine as build_python

COPY . /src

WORKDIR /src
RUN apk -U add git gcc musl-dev \
    && pip install virtualenv \
    && virtualenv --no-site-packages /venv \
    && /venv/bin/python setup.py install





FROM python:2-alpine
ENV CONFD_VERSION 0.14.0

COPY --from=build_python /venv /venv
COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh
COPY ./confd /etc/confd

RUN apk -U add curl openssl openssh-client jq \
    && /venv/bin/pip --no-cache-dir install gunicorn \
    && curl -sSL https://github.com/kelseyhightower/confd/releases/download/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64 -o /usr/local/bin/confd \
    && chmod +x /usr/local/bin/confd \
        /usr/local/bin/entrypoint.sh \
    && mkdir -p /etc/ca-server \
        /var/log/ca-server

EXPOSE 8080
VOLUME [ "/etc/ca-server/cas" ]
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]
CMD ["gunicorn", "-w", "4", "ssh_ca_server:app", "-b", "0.0.0.0:8080"]
