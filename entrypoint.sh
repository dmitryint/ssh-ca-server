#!/bin/sh
set -e

export LDAP_SERVER=${LDAP_SERVER:-ldap.acme.com}
export LDAP_SERVER_PORT=${LDAP_SERVER_PORT:-389}
export LDAP_DOMAIN=${LDAP_DOMAIN:-acme.com}
export LDAP_BIND_USER=${LDAP_BIND_USER:-bind-user}
export LDAP_BIND_PASS=${LDAP_BIND_PASS:-bind-password}
export LDAP_BASE_DN=${LDAP_BASE_DN:-DC=acme,DC=com}
export LDAP_GROUP_DN=${LDAP_GROUP_DN:-DC=acme,DC=com}
export LDAP_ROLE_ATTRIBUTE=${LDAP_ROLE_ATTRIBUTE:-extensionAttribute1}
export LDAP_ROLE_DESCRIPTION=${LDAP_ROLE_DESCRIPTION:-description}
export LDAP_CA_ATTRIBUTE=${LDAP_CA_ATTRIBUTE:-extensionAttribute2}
export LDAP_PRINCIPAL_ATTRIBUTE=${LDAP_PRINCIPAL_ATTRIBUTE:-extensionAttribute3}
export CAS_PROD_MAX_DURATION=${CAS_PROD_MAX_DURATION:-24h}
export CAS_NONPROD_MAX_DURATION=${CAS_NONPROD_MAX_DURATION:-30d}
export CAS_TEST_MAX_DURATION=${CAS_TEST_MAX_DURATION:-1h}

/usr/local/bin/confd -backend="env" -confdir="/etc/confd" -onetime  --log-level=debug

for ca_name in $(cat /etc/ca-server/config.json |jq -r '.cas[].name'); do
    if [ ! -f "/etc/ca-server/cas/${ca_name}" ]; then
        ssh-keygen -t rsa -b 4096 -N "" -f /etc/ca-server/cas/${ca_name}
    fi
done

source /venv/bin/activate
exec "$@"
